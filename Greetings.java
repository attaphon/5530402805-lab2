package kitteerapakorn.attaphon.lab2; 
public class Greetings 
{
	public static void main(String[] args) 
	{
		if(args.length == 3)
		{
			System.out.println("My favorite teacher's name is " + args[0]);
			System.out.println("The teacher taught me at " + args[1] + " which is in " + args[2]);
		}
		else System.err.print("Greeting <teacher name> <school name> <province name>");
	}
}
