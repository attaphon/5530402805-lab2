package kitteerapakorn.attaphon.lab2;

public class AreaVolumeConeSolver 
{
	public static void main(String[] args)
	{
		if (args.length==3)
		{
				/////Processing and print/////
				double r = Double.parseDouble(args[0]);
				double s = Double.parseDouble(args[1]);
				double h = Double.parseDouble(args[2]);
				System.out.println("For cone with r " + r + " s " + s + " h " + h);
				System.out.println("Surface area is " + ((Math.PI * r * s) + (Math.PI * r * r)) + " Volume is " + (1.0/3 * Math.PI * r * r * h) );
		}
		else System.err.println("AreaVolumeConeSlove <r> <s> <h>");	
	}
}
