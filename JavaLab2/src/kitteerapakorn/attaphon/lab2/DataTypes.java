package kitteerapakorn.attaphon.lab2;

public class DataTypes 
{
	public static void main(String[] args)
	{
		/////Variable/////
		String a1 = args[0];
		String a2 = args[1];
		String a3 = a1.substring(0,1);
		String pa = a2.substring(0,2);
		String sa = a2.substring(8,10);
		boolean a = true;		
		
		/////Processing/////
		int x = Integer.parseInt(sa);
		String oct = Integer.toOctalString(x);
		String hex = Integer.toHexString(x);
		double ans3 = x + (Integer.parseInt(pa)/100);
		long ans1 = x + (Integer.parseInt(pa)/100);
		float ans2 = x + (Integer.parseInt(pa)/100);
		
		/////Print/////
		System.out.println("My name is " + a1);
		System.out.println("My student ID was " + a2);
		System.out.println(a3 +" " + a + " " + oct + " " + hex);
		System.out.print(ans1 + " " + ans2 + " " + ans3);
		
	}
}
