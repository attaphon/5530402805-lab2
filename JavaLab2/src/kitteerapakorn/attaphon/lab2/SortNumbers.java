package kitteerapakorn.attaphon.lab2;

public class SortNumbers {
	public static void main(String[] args)
	{	
		/////Variable/////
		float[] x = new float[4];
		x[0] = Float.parseFloat(args[0]); 
		x[1] = Float.parseFloat(args[1]);
		x[2] = Float.parseFloat(args[2]);
		x[3] = Float.parseFloat(args[3]);

		System.out.println("For the input numbers:\n" + x[0] + " " + x[1] + " " + x[2] + " " + x[3]);
		
		/////Processing/////
		for(int i=0 ; i<4 ; i++)
		{
			for(int l=0 ; l<4 ; l++)
			{
				if(x[l] > x[i])
				{
					float temp = x[l];
					x[l] = x[i];
					x[i] = temp;
				}
			}
		}
		
		System.out.println("Sorted numbers:\n" + x[0] + " " + x[1] + " " + x[2] + " " + x[3]);
	}
}
