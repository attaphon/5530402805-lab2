package kitteerapakorn.attaphon.lab2;

public class UsingStringAPI 
{
	public static void main(String[] args)
	{
		/////Variable/////
		String schoolName = args[0];
		int college,school;
	
		/////Search/////
		college = schoolName.indexOf("College");
		school = schoolName.indexOf("School");
	
		/////Processing and print/////
		if(college >= 0)
		{
			System.out.println(schoolName + " has college at " + college);
		}
		else if(school >= 0)
		{
			System.out.println(schoolName + " has school at " + school);
		}
		else 
		{
			System.out.println(schoolName + " does not contain school or college");
		}		
	}

}
